<?php
class MysqlConnection
{
    const HOST = '';
    const USER = '';
    const PASS = '';
    const DB_NAME = '';
    const CHARSET = 'utf8';

    protected mysqli $connection;
    public string $errorMessage = '';

    public function __construct()
    {
        $this->connection = new mysqli(
            self::HOST,
            self::USER,
            self::PASS,
            self::DB_NAME,
        );

        if ($this->connection->connect_error) {
            $this->errorMessage .= 'Failed to connect to MySQL - ' . $this->connection->connect_error;
        }
        $this->connection->set_charset(self::CHARSET);
    }

    public static function get():mysqli
    {
        $entity = new static();
        return $entity->connection;
    }
}