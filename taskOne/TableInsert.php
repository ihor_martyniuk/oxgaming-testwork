<?php

require_once('TableInsertInterface.php');
require_once('AbstractCommon.php');

/**
 * Class TableInsert
 */
class TableInsert extends AbstractCommon implements TableInsertInterface
{
    /**
     * @var string
     */
    protected string $row;

    /**
     * @var string
     */
    protected string $rowKey;

    /**
     * TableInsert constructor.
     * @param string $row
     */
    public function __construct(string $row)
    {
        $this->row = $this->escapeIdent($row);
        $this->rowKey = md5($this->row);
        parent::__construct();
    }

    /**
     * @return bool
     */
    public function insert(): bool
    {
        $result = true;
        $query = "
            INSERT IGNORE INTO Book (row_key, row) VALUES (?, ?)
        ";
        $preRequest = $this->connection->prepare($query);
        $preRequest->bind_param("ss", $this->rowKey, $this->row); // for security reasons

        if (!$preRequest->execute()) {
            $this->errorMessage .= $preRequest->error;
            $result = false;
        }

        $this->connection->close();

        return $result;
    }

    /**
     * @param $row
     * @return string
     */
    protected function escapeIdent($row)
    {
        return "`".str_replace("`","``",$row)."`";
    }
}