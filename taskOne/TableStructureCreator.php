<?php

require_once('TableStructureCreatorInterface.php');
require_once('./workers/MysqlConnection.php');
require_once('AbstractCommon.php');

/**
 * Class TableStructureCreator
 */
class TableStructureCreator extends AbstractCommon implements TableStructureCreatorInterface
{

    /**
     * @return bool
     */
    public function create(): bool
    {
        $result = true;

        $createQuery = "
            CREATE TABLE  Book(
                row_key VARCHAR(32)  NOT NULL,
                row MEDIUMTEXT NOT NULL,
                created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`row_key`)
            );
        ";

        if (!$this->connection->query($createQuery)) {
            $this->errorMessage .= $this->connection->error;
            $result = false;
        }

        $this->connection->close();

        return $result;
    }
}