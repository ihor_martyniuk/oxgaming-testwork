<?php

require_once('ErrorInterface.php');


/**
 * Interface TableInsertInterface
 */
interface TableInsertInterface extends ErrorInterface
{
    /**
     * @return bool
     */
    public function insert(): bool ;
}