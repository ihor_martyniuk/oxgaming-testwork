<?php


abstract class AbstractCommon
{
    /**
     * @var mysqli
     */
    protected mysqli $connection;
    /**
     * @var string
     */
    protected string $errorMessage = '';

    /**
     * TableStructureCreator constructor.
     */
    public function __construct()
    {
        $this->connection = MysqlConnection::get();
    }

    /**
     * @return string
     */
    public function getError(): string
    {
        return $this->errorMessage;
    }
}