<?php

require_once('ErrorInterface.php');


/**
 * Interface TableCreatorInterface
 */
interface TableStructureCreatorInterface extends ErrorInterface
{
    /**
     * @return bool
     */
    public function create(): bool ;
}