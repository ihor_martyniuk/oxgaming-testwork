<?php


/**
 * Interface ErrorInterface
 */
interface ErrorInterface
{
    /**
     * @return string
     */
    public function getError(): string ;
}