<?php


require_once('TableStructureCreator.php');
require_once('TableStructureCreatorInterface.php');
require_once('TableInsert.php');
require_once('TableInsertInterface.php');


class TaskManager
{
    protected TableStructureCreatorInterface $tableCreate;
    protected TableInsertInterface $tableInsert;

    public function __construct()
    {
        $this->tableCreate = new TableStructureCreator();
    }

    public function createTable(): bool
    {
        return $this->tableCreate->create();
    }

    public function insert(string $content): bool
    {
        $this->tableInsert = new TableInsert($content);
        return $this->tableInsert->insert();
    }
}