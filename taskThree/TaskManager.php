<?php


require_once('./workers/RedisConnection.php');

/**
 * Class TaskManager
 */
class TaskManager
{
    /**
     * @var RedisConnection
     */
    protected RedisConnection $connection;

    /**
     * @var string
     */
    protected string $login;

    /**
     * @var string
     */
    protected string $pass;

    /**
     * @var string
     */
    protected string $errorMessage = '';

    /**
     * TaskManager constructor.
     * @param string $login
     * @param string $pass
     */
    public function __construct(string $login, string $pass)
    {
        $this->login = $login;
        $this->pass = $pass;
        $this->connection = RedisConnection::get();
    }

    /**
     * @return bool
     */
    public function signIn(): bool
    {
        $result = false;

        if (!$this->isExist()) {
            $this->errorMessage .= 'Login or pass does not correct!';
        } else {
            $storedPass = $this->connection->execute("
                HGET " . $this->login . "
                pass " . $this->pass . "
            ");

            if ($storedPass && $storedPass == $this->pass) {
                $result = true;
            }
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function signUp()
    {
        $result = false;

        if ($this->isExist()) {
            $this->errorMessage .= 'Login or pass does not correct!';
        } else {
            $queryResult = $this->connection->execute("
                HSET " . $this->login .
                " login " . $this->login . "
                pass " . $this->pass . "
            ");

            if ($queryResult && $queryResult == 'OK') {
                $result = true;
            }
        }

        return $result;
    }

    /**
     * @return bool
     */
    protected function isExist(): bool
    {
        $result = true;

        $res = $this->connection->execute("
            HEXISTS " . $this->login . " login
        ");
        if (!$res) {
            $result = false;
        }

        return $result;
    }
}