<?php


require_once('./workers/MysqlConnection.php');

/**
 * Class TaskManager
 */
class TaskManager
{

    /**
     * @var int
     */
    protected int $intervalDays = 30;

    /**
     * @var string
     */
    protected string $originTableName = 'origin';

    /**
     * @var string
     */
    protected string $archiveTableName = 'archive';

    /**
     * @return bool
     */
    public function executeArchive(): bool
    {
        $result = true;

        $connection = MysqlConnection::get();

        $connection->begin_transaction();

        $query = "
            INSERT INTO :archiveTable
            SELECT *
            FROM :originTable
            WHERE  created_at > NOW() - INTERVAL :intervalDays DAY;
            DELETE FROM :originTable WHERE created_at > NOW() - INTERVAL :intervalDays DAY;
        ";

        $preRequest = $connection->prepare($query);
        $preRequest->bind_param(':archiveTable', $this->archiveTableName);
        $preRequest->bind_param(':originTable', $this->originTableName);
        $preRequest->bind_param(':intervalDays', $this->intervalDays);

        if (!$preRequest->execute()) {
            $result = false;
            $connection->rollback();
        } else {
            $connection->commit();
        }

        return $result;
    }
}